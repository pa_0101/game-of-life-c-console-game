﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class GameOfLife : IGameOfLife
    {
        public GameOfLife() { }
        public GameOfLife(int height, int width)
        {
            try
            {
                Height = height;
                Width = width;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid arguments.");
            }          
        }

        public int Height { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }
        public bool IsAlive { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }

        GameOfLife gameBoard = null;
        Rules rules = new Rules();
        JSON gameState = null;
        GameOfLife gameOfLife = null;



        /* SelectTemplate() prompts the user to select a template
         * from the list and returns the file name. */
        public string SelectTemplate()
        {
            var input = string.Empty;
            var isFound = false;
            var templatesIndex = 0;
            var selectedFile = string.Empty;
            var isValidInput = true;
            var templateNum = 0;
            var numberInput = string.Empty;

            // Get the templates from the directory folder
            // and load them into the templates array.
            var templates = Directory.GetFiles("Templates");
            do
            {
                Console.Write("\nSelect a template: ");
                numberInput = Console.ReadLine();
                Console.WriteLine();
                isValidInput = int.TryParse(numberInput, out templateNum);

                if (templateNum < 1 || templateNum > templates.Length)
                {
                    Console.WriteLine($"Make a selection from 1 to {templates.Length}.");
                    isValidInput = false;
                }
            } while (!isValidInput);

            // Loop through the templates array and find a template
            // at the index that matches the users selection.
            for (int i = 1; i < templates.Length + 1; i++)
            {
                if (templateNum == i)
                {
                    isFound = true;
                    templatesIndex = i - 1;
                    break;
                }
            }
            if (isFound == true)
            {
                // Path.GetFileName() gives us the file name without
                // the path of the folder that contains it.
                selectedFile = Path.GetFileName(templates[templatesIndex]);
            }
            return selectedFile;
        }



        /* GetTemplateData() gets all the data from a template
         * file such as the width, height and the Cells 2d array
         * to be parsed into Template class to create a template object. 
         * Further validation needs to be completed.*/
        public Template GetTemplate(string fileName)
        {
            var heightAsString = string.Empty;
            var widthAsString = string.Empty;
            var height = 0;
            var width = 0;
            var path = "../Debug/Templates/" + fileName;
            List<char> ch = new List<char>();

            try
            {
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException();
                }

                // Get the height and width values from the file.
                using (StreamReader file = new StreamReader(path))
                {
                    // 1st line.
                    heightAsString = file.ReadLine();
                    // 2nd line.
                    widthAsString = file.ReadLine();
                }
                // Convert width and height strings to integers.
                int.TryParse(heightAsString, out height);
                int.TryParse(widthAsString, out width);

                // Get the template cells from the file starting from the
                // 3rd row (where the template cells line start), down to
                // the end of the last row, which is height amount of rows in the file.
                var lines = System.IO.File.ReadLines(path).Skip(2).Take(height).ToArray();

                // Here we loop through each line and get each character from the cells 
                // in the template list and add it into the ch List.   
                char[] characters = new char[width];

                for (int i = 0; i < height; i++)
                {
                    characters = lines[i].ToCharArray();
                    for (int j = 0; j < characters.Length; j++)
                    {
                        ch.Add(characters[j]);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file is not found in the specified location");
                return null;
            }
            Template myTemplate = CreateTemplateObject(fileName, height, width, ch);
            return myTemplate;
        }



        /* CreateTemplateObject() creates a Template object with the 
         * data retrieved from the template.txt file. */
        public Template CreateTemplateObject(string fileName, int height,
                                       int width, List<char> chars)
        {
            var index = 0;
            // Loop through the char List and add all the chars back into
            // the Cell 2d array.
            Cell[][] cells = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                Cell[] innerCells = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    if (chars[index].Equals('O'))
                    {
                        innerCells[j] = Cell.Alive;
                    }
                    if (chars[index].Equals('X'))
                    {
                        innerCells[j] = Cell.Dead;
                    }
                    index++;
                }
                // Assign the innerCells array to the current cells index.
                cells[i] = innerCells;
            }
            // Create a Template object and return it to the caller
            // to be used for a game.
            Template myTemplate = new Template(fileName, height, width, cells);
            return myTemplate;
        }



        /* InitializeGame() initializes a new game to a new state with height,
         * width, and coordinate points for a game template. It also makes a 
         * function call to initialize the game board and insert a template 
         * onto the game board. */
        public void InitializeGame(Template template)
        {
            var isCoord = false;
            var height = template.Height;
            var heightPrompt = $"\nEnter game height (must be at least {height}): ";
            var width = template.Width;
            var widthPrompt = $"\nEnter game width (must be at least {width}): ";

            var heightResult = GameInitializeInput(height, heightPrompt, isCoord);
            var widthResult = GameInitializeInput(width, widthPrompt, isCoord);

            // Calculate coordinate boundaries.
            var xBoundry = widthResult - template.Width;
            var yBoundry = heightResult - template.Height;

            var xBoundryPrompt = $"\nEnter template x coordinate (cannot be more than {xBoundry}): ";
            var yBoundryPrompt = $"\nEnter template y coordinate (cannot be more than {yBoundry}): ";
            isCoord = true;

            var xCoordResult = GameInitializeInput(xBoundry, xBoundryPrompt, isCoord);
            var yCoordResult = GameInitializeInput(yBoundry, yBoundryPrompt, isCoord);

            // Keep a reference to the size of the gameboard and
            // the location of where we inserted the game. We need this
            // for when we save our game state.
            Height = heightResult;
            Width = widthResult;
            CoordX = xCoordResult;
            CoordY = yCoordResult;
            
            // Create a GameOfLife object.
            gameBoard = new GameOfLife(heightResult, widthResult);
            // Initialize the game grid.
            gameBoard.Cells = InitializeGameGrid(heightResult, widthResult);
            // Insert a template into the Game of Life.
            InsertTemplate(template, xCoordResult, yCoordResult);
        }



        /* GameInitializeInput() reads in numeric input from the user and
         * validates it until correct input is entered.*/
        public int GameInitializeInput(int value, string prompt, bool isCoord)
        {
            var input = string.Empty;
            var result = 0;
            var isValidInput = true;

            do
            {
                Console.Write(prompt);
                input = Console.ReadLine();
                Console.WriteLine();
                isValidInput = int.TryParse(input, out result);

                if ((result < value || !isValidInput) && !isCoord)
                {
                    Console.WriteLine("Invalid input.");
                    isValidInput = false;
                }
                if ((result > value || result < 0 || !isValidInput) && isCoord)
                {
                    Console.WriteLine("Invalid input.");
                    isValidInput = false;
                }
            } while (!isValidInput);
            return result;
        }



        /* InitializeGameGrid() initializes the game board 
         * to an empty(Dead) state for us to insert the game template. */
        public Cell[][] InitializeGameGrid(int height, int width)
        {
            Cell[][] gameGrid = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                Cell[] gameRow = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    gameRow[j] = Cell.Dead;
                }
                gameGrid[i] = gameRow;
            }
            return gameGrid;
        }



        /* InsertTemplate() inserts a game template onto an empty 
         * game board at a coordinate chosen by the user. */
        public void InsertTemplate(ITemplate template, int templateX, int templateY)
        {
            var row = 0;
            Console.WriteLine();

            // Keep track of where the template was inserted.
            CoordX = templateX;
            CoordY = templateY;

            Cell[][] templateCells = template.Cells;
            if (gameBoard != null)
            {
                for (int i = 0; i < gameBoard.Height; i++)
                {
                    for (int j = 0; j < gameBoard.Width; j++)
                    {
                        // Here we are displaying the initial state of the game.
                        // We loop through the game board and when we get to the
                        // coordinate chosen by the user, we print the row that corresponds
                        // to the row and k counter variables from the template Cell array.
                        if (i == templateY && j == templateX)
                        {
                            for (int k = 0; k < template.Width; k++)
                            {
                                if (template.Cells[row][k] == Cell.Alive)
                                {
                                    IsAlive = true;
                                    Console.Write(ToString());
                                }
                                if (template.Cells[row][k] == Cell.Dead)
                                {
                                    IsAlive = false;
                                    Console.Write(ToString());
                                }
                            }
                            // Here we check if we have hit the last row of the template Cell array.
                            if (row != template.Height - 1)
                            {
                                row++;
                                templateY++;
                            }
                            // Increment the j counter variable by the templates width
                            // so we can finish off the rest of the game board row.
                            j += template.Width;
                        }
                        // If j is less than the game board width, check if its a Dead cell
                        // and print space.
                        if (j < gameBoard.Width)
                        {
                            if (gameBoard.Cells[i][j] == Cell.Dead)
                            {
                                Console.Write(' ');
                            }
                        }
                    }
                    Console.WriteLine();
                }
            }
            gameOfLife = new GameOfLife();
            gameOfLife.Cells = template.Cells;
        }



        /* PlayGame() method handles the cancellation of the aSynchronous
         * PlayGameAsync() method. It also communicates that cancellation 
         * has been handled. 
         * Code on lines 350 to 367 provided by Matthew Bolger. 
         * (Week 3 chat collaborate). Accessed 16th June 2016. */
        public async void PlayGame()
        {
            // "A CancellationTokenSource object, which provides a cancellation token through 
            // its Token property and sends a cancellation message by calling its Cancel or 
            // CancelAfter method." 
            // https://msdn.microsoft.com/en-us/library/system.threading.cancellationtokensource(v=vs.110).aspx

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            Task playGameTask = PlayGameAsync(cancellationTokenSource.Token);

            // Wait for user to stop the game.
            Console.ReadKey(true);
            // Cancel() communicates a request for cancellation.
            cancellationTokenSource.Cancel();

            // await keyword suspends the execution of a method 
            // until the awaited task completes.
            await playGameTask;

            // Create a JSON gamestate object.
            gameState = new JSON(Height, Width, CoordX, CoordY, gameOfLife.Cells);
            gameState.SaveToJSON(gameState);
            Console.WriteLine("\nGame stopped - the state has been saved.");
        }



        /* PlayGameAsync() method continuously calls our TakeTurn() method
         * and returns a task value when work is suspeneded. 
         * Code on lines 381 to 405 provided by Matthew Bolger. 
         * (Week 3 chat collaborate). Accessed 16th June 2016. */
        private async Task PlayGameAsync(CancellationToken cancellationToken)
        {
            // While cancellation has not been requested 
            // (user has not pressed a key to trigger cancellation).
            while (!cancellationToken.IsCancellationRequested)
            {
                Console.Write("Press any key to stop game...");
                try
                {
                    if (gameOfLife.Cells != null)
                    {
                        // await suspends progress for 1 second.
                        // Call TakeTurn() every 1 second game tick.
                        await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
                        // Clear the buffer in the console for next generation.
                        Console.Clear();
                        gameOfLife.Cells = rules.GameRules(gameOfLife.Cells);
                        if (gameOfLife.Cells.Equals(null))
                        {
                            throw new ArgumentException();
                        }
                        TakeTurn();
                    }
                }
                catch (Exception)
                {

                };
            }
        }



        /* TakeTurn() is called at each game tick and it delegates,
         * calls functions that are needed to update the game cells,
         * print the newest update and prepare a new input copy for 
         * the next tick.*/
        public void TakeTurn()
        {
            // Apply the game rules to the cells.
            //gameOfLife.Cells = rules.GameRules(gameOfLife.Cells);

            // Print the new generation.
            PrintGameTransition(gameOfLife.Cells);
        }



        /* PrintGameTransition() prints the output of the game cells that 
         * have had the game rules applied to them. */
        public void PrintGameTransition(Cell[][] arr)
        {
            var height = arr.GetLength(0);
            var width = arr[0].Length;
            var row = 0;
            var coordXCopy = CoordX;
            var coordYCopy = CoordY;

            // We loop through the game board and when we get to the
            // coordinate chosen by the user when the template was inserted, 
            // we print the row that corresponds to the row and k counter 
            // variables from the template Cell array.
            for (int i = 0; i < gameBoard.Height; i++)
            {
                for (int j = 0; j < gameBoard.Width; j++)
                {
                    if (i == coordYCopy && j == coordXCopy)
                    {
                        for (int k = 0; k < width; k++)
                        {
                            if (arr[row][k] == Cell.Alive)
                            {
                                IsAlive = true;
                                Console.Write(ToString());
                            }
                            if (arr[row][k] == Cell.Dead)
                            {
                                IsAlive = false;
                                Console.Write(ToString());
                            }
                        }
                        // Here we check if we have hit the last row of the passed in array.
                        if (row != height - 1)
                        {
                            row++;
                            coordYCopy++;
                        }
                        // Increment the j counter variable by the games width
                        // so we can finish off the rest of the game board row.
                        j += width;
                    }
                    // If j is less than the game board width, check if its a Dead cell
                    // and print space.
                    if (j < gameBoard.Width)
                    {
                        if (gameBoard.Cells[i][j] == Cell.Dead)
                        {
                            Console.Write(' ');
                        }
                    }
                }
                Console.WriteLine();
            }
        }



        /* ToString() prints the dead or alive cells on the game board. */
        public override string ToString()
        {
            if (IsAlive == true)
            {
                return "\u2588";
            }
            return " ";
        }



        /* ResumeGame() returns a copy of our saved JSON file (game state).
         * We then get all the attributes of the last game state from the
         * JSON file which we use to re-initialize the gameboard, re-instantiate
         * a GameOfLife and resume the last game that was played. */
        public void ResumeGame()
        {
            JSON json = new JSON();
            JSON jsonObject = json.DeserializeJSON();

            var height = jsonObject.Height;
            var width = jsonObject.Height;
            CoordX = jsonObject.XCoord;
            CoordY = jsonObject.YCoord;

            // Keep a record of the size of the gameboard. We need this
            // to persist for when we re-initialize the game.
            Height = height;
            Width = width;

            // Re-create a new game of life
            gameBoard = new GameOfLife(height, width);
            // Re-initialize the gameboard grid.
            gameBoard.Cells = InitializeGameGrid(height, width);

            // De-serealize the saved game state and resume the game.
            gameOfLife = new GameOfLife();
            gameOfLife.Cells = jsonObject.GameStateCells;
            PrintGameTransition(gameOfLife.Cells);
            PlayGame();
        }
    }
}
