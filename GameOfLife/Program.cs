﻿using System;

namespace GameOfLife
{
    public class Program
    {
        private static void Main()
        {
            Menu menu = new Menu();
            Template template = new Template();
            GameOfLife gameOfLife = new GameOfLife();
            var mainMenuInput = 0;

            do
            {
                menu.ShowMainMenu();
                mainMenuInput = menu.GetMenuOptionInput();
                switch (mainMenuInput)
                {
                    case 1:
                        Template myTemplate = template.CreateTemplate();
                        myTemplate.WriteTemplateToFile();
                        break;
                    case 2:
                        menu.ShowGameMenu();
                        var gameMenuInput = menu.GetMenuOptionInput();
                        // Check if "New game" selected.
                        if (gameMenuInput == 1)
                        {
                            var isTemplatesFound = template.ListAllTemplates();
                            // If no templates are found, then continue on to main menu.
                            if (!isTemplatesFound)
                            {
                                continue;
                            }
                            var file = gameOfLife.SelectTemplate();
                            Template myGameTemplate = gameOfLife.GetTemplate(file);
                            // If game template not found, then continue on to main menu. 
                            if (myGameTemplate == null)
                            {
                                continue;
                            }
                            else
                            {
                                myGameTemplate = myGameTemplate.DisplayTemplate(myGameTemplate);
                            }
                            gameOfLife.InitializeGame(myGameTemplate);
                            gameOfLife.PlayGame();
                        }
                        // Check if "Resume game" selected.
                        if (gameMenuInput == 2)
                        {
                            gameOfLife.ResumeGame();
                        }
                        break;
                    case 3:
                        break;
                }
            } while (mainMenuInput != 3);

            Console.WriteLine("\nGoodbye!");
        }
    }
}
